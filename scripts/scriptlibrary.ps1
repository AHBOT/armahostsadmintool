﻿#Script Library

function Get-SteamCollectionContentIDs{
param (
$collection
)
#STEAM_GetCollectionContentIDs.ps1
$URI = 'https://steamcommunity.com/sharedfiles/filedetails/?id=' + $collection
$HTML = (Invoke-WebRequest -Uri $URI).Links

$modID = ($HTML | Where-Object {($_.href -like “https://steamcommunity.com/sharedfiles/filedetails/?id=*”)-and ($_.innertext -like "")}).href

 foreach ($item in $modID) { ($item.Substring(55,$item.Length-55))}
}

function Connect-MySQL([string]$user, [string]$pass, [string]$MySQLHost, [string]$database) { 
    # Load MySQL .NET Connector Objects 
    [void][system.reflection.Assembly]::LoadWithPartialName("MySql.Data") 
 
    # Open Connection 
    $connStr = "server=" + $MySQLHost + ";port=3306;uid=" + $user + ";pwd=" + $pass + ";database="+$database+";Pooling=FALSE" 
    try {
        $conn = New-Object MySql.Data.MySqlClient.MySqlConnection($connStr) 
        $conn.Open()
    } catch [System.Management.Automation.PSArgumentException] {
         "Unable to connect to MySQL server, do you have the MySQL connector installed..?"
         $_
        Exit
    } catch {
         "Unable to connect to MySQL server..."
         $_.Exception.GetType().FullName
         $_.Exception.Message
        exit
    }
     "Connected to MySQL database $MySQLHost\$database"

    return $conn 
}
