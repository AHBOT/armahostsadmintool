﻿#Gets variables from the configuration file
. C:\arma3\scripts\serverconfig.ps1

#Sets primary foldername for the new server
$ServiceDir = $arma3root + '\' + $Servers[-1]

#Creates sub-directories in new server instance
forEach ($line2 in $createfolders){
New-Item -Path ($serviceDir + '\' + $line2) -ItemType Directory
}
#Links folders from master to new server instance
forEach ($line2 in $symfolders) {
Write-Host "Linking " + ($ServiceDir +'\'+ $line2) + " from " + ($masterdir + '\' + $line2)
New-Item -Path ($ServiceDir + '\' + $line2) -ItemType SymbolicLink -Value ($masterdir + '\' + $line2)
} 
#Links files from master to new server instance
forEach ($line2 in $symfiles) {
Write-Host "Linking " + ($ServiceDir +'\'+ $line2) + " from " + ($masterdir + '\' + $line2)
New-Item -Path ($ServiceDir + '\' + $line2) -ItemType SymbolicLink -Value ($masterdir + '\' + $line2)
} 
#Links optional keys from master to server instance
forEach ($line2 in (Get-ChildItem ($masterdir + '\keys') -Exclude 'a3.bikey')){
Write-Host "Linking " + ($ServiceDir +'\'+ $line2.Name) + " from " + ($line2.FullName)
New-Item -Path ($ServiceDir + '\' + $line2.Name) -ItemType SymbolicLink -Value ($line2.FullName)
}