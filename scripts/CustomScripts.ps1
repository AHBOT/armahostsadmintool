﻿. C:\arma3\scripts\scriptlibrary.ps1
. C:\arma3\scripts\serverconfig.ps1
param($request)

switch($request){
"AltisLife" {
#Config
$server = Read-Host "Which server would you like to work with?"
$mission = Read-Host "Which mission would you like? [Altis/Tanoa]"
$database = Read-Host "Do you have a database configured? [Y/N]"

if($database -eq "N"){
.CustomScripts.ps1 "MySQL"
}
wget https://github.com/AsYetUntitled/Framework/archive/master.zip -OutFile $steamroot\lifemaster.zip
Expand-Archive -Path $steamroot\lifemaster.zip -DestinationPath $steamroot\lifemaster\
$lifedir = "$steamroot\lifemaster\Framework-master"
Copy-Item -Path "$lifedir\life_server" -Destination "$arma3root\$server\@lifeserver\addons\" -Force -Recurse
Copy-Item -Path $lifedir\befilters\* -Destination $arma3root\$server\battleye\ -Force
Copy-Item -Path "$lifedir\Altis_Life.Altis\*" -Destination "$arma3root\$server\mpmissions\life.$mission" -Force -Recurse
Copy-Item -Path "$lifedir\SQMs\mission$mission.sqm" -Destination "$arma3root\$server\mpmissions\life.$mission\mission.sqm" -Force
wget https://www.7-zip.org/a/7z1806.exe -OutFile $steamroot\7z.exe
&$steamroot\7z.exe

wget https://dev.mysql.com/get/Downloads/Connector-Net/mysql-connector-net-8.0.14.msi -OutFile $steamroot\mysqlconnect.msi
& msiexec /i $steamroot\mysqlconnect.msi /qb
$pw = Read-Host "What is your MySQL Root password?"

#Create Database
$mysqlsource = "$lifedir\altislife.sql"
$mysqlbin = 'C:\Program Files (x86)\MySQL\MySQL Server 5.7\bin'
$parameter = '/c mysql --user=root --password="'+$pw+'" < '+$mysqlsource
Set-Location -Path $mysqlbin
Start-Process cmd -ArgumentList $parameter
"Database Created"

Set-Location -Path 'C:\Program Files (x86)\7-Zip'
wget https://bitbucket.org/torndeco/extdb3/downloads/extDB3-1031.7z -OutFile $steamroot\extDB3.7z
Start-Process cmd -ArgumentList "/c 7z x $steamroot\extDB3.7z -o$arma3root\$server\"
$replace = @"
Username = changeme
Password =  changeme
Database = changeme
"@
$replacement = @"
Username = root
Password = $pw
Database = altislife
"@
((Get-Content -path $arma3root\$server\@extDB3\extdb3-conf.ini -Raw) -replace $replace,$replacement) | Set-Content -Path $arma3root\$server\@extDB3\extdb3-conf.ini
}

"MySQL" { #Install MySQL
$password = 'passwd="' + [system.web.security.membership]::GeneratePassword(32,1) + '"'
wget http://download.microsoft.com/download/0/5/6/056dcda9-d667-4e27-8001-8a0c6971d6b1/vcredist_x64.exe -OutFile $steamroot\vcredist_x64_2013.exe
& $steamroot\vcredist_x64_2013.exe -silent
wget https://dev.mysql.com/get/Downloads/MySQLInstaller/mysql-installer-community-8.0.14.0.msi -OutFile $steamroot\mysql.msi
& msiexec /i $steamroot\mysql.msi /qb
$mysqlinstall = "C:\Program Files (x86)\MySQL\MySQL Installer for Windows\MySQLInstallerConsole.exe"
$version = "server;5.7.21;X64:*:port=3306;openfirewall=true;$password"
& $mysqlinstall install $version -silent
$pw = $password -replace "passwd=" -replace '"'
$dbinfo = @"
=============================
Your MySQL Credentials
Host: localhost
Username: root
Password: $pw

You may also look it up at:
$steamroot\mysqldb.info
=============================
"@
$dbinfo
$dbinfo | Out-File $steamroot\mysqldb.info
}

default {}

}
