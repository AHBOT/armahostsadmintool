﻿. C:\arma3\scripts\serverconfig.ps1

#Symlink mods to servers
for ($i = 0;$i -lt ($Servers.Count);$i++){
$servicedir = $arma3root +'\'+ $servers[$i]
#Cleanup old mods
$removemods = Get-ChildItem -Path $servicedir -Directory -filter "@*"
if($removemods.count -gt 0){
foreach ($mod in $removemods){
[io.directory]::Delete($mod.FullName)
}
}else{}
#Cleanup old keys
$removekeys = Get-ChildItem -Path ($servicedir + '\keys') -Exclude "a3.bikey"
if($removekeys.count -gt 0){
foreach ($key in $removekeys){
Remove-Item -Path $key.fullname -Force
}
}else{}
"Adding mods to $servicedir"
($ServerMods[1][0])
for ($idx = 0; $idx -lt ($ServerMods[$i]).count;$idx++){
    $mod = $ServerMods[$i][$idx]
    $modid = $mod[1]
    $modname = $mod[0]
    Write-Host "Linking " + ($ServiceDir +'\@'+ $modname) + " from " + ($moddir + '\' + $modid)
    New-Item -Path ($ServiceDir + '\@' + $modname) -ItemType SymbolicLink -Value ($moddir + '\' + $modid)
    #Add bikeys to the keys folder
    foreach ($item in (Get-ChildItem -Path ($ServiceDir + '\@' + $modname) -Recurse -Filter "*.bikey")){
        Copy-Item -Path $item.fullname -Destination ($servicedir + '\keys')
    }
    }
foreach ($item in (Get-ChildItem -Path ($masterdir + '\keys') -Recurse -Exclude "a3.bikey")){
    Copy-Item -Path $item.fullname -Destination ($servicedir + '\keys')
}
}
