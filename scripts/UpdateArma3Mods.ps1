﻿. C:\arma3\scripts\scriptlibrary.ps1
. C:\arma3\scripts\serverconfig.ps1

#Look for file to get mods
"Steamworkshop html files:"
try{
(Get-ChildItem -Path $arma3root\modtemp\ -Filter *.html).name
Set-Clipboard (Get-ChildItem -Path $arma3root\modtemp\ -Filter *.html).name
} catch {"No HTML File"}
""
$result = Read-Host "Enter Name of modlist: [Leave BLANK to SKIP]"
if(!($result -eq "")) {
    $file = ((Get-Content -Path ("$arma3root\modtemp\" + "$result")) -replace "`t" -replace " " | ? {$_ -like "*http://steamcommunity.com/sharedfiles/filedetails/?id=*" }) 
    foreach($item IN $file) {
    $modIDList += @(($item -split "=")[4] -replace "</a>")
    }
}

#Look for steam collection
$result = Read-Host "Enter a collection ID: [Leave BLANK to SKIP]" 
if(!($result -eq "")) {
$resultList = Get-SteamCollectionContentIDs -collection $result
    foreach($item in $resultList) {
    $modIDList += @($item)
    }
}

do 
{
$modID = (Read-Host -Prompt "Enter the mod id: (Leave BLANK to exit)") -replace "[^0-9]"
if(!($modID -eq "")){$modIDList+= @($modID)}

$strQuit = if($modID -eq ''){"n"}else{"y"}
               
}
while($strQuit -eq 'y')

"You added " + $modIDList.Count + " mods look up. Here is valid list:"
$modIDList


$workshopsmods = foreach ($item IN $modIDList) {
    "workshop_download_item 107410 $item `r`n"
}
$workshopScript = @"
$steamCredential
bVerifyAllDownloads 1
$workshopsmods
exit
"@
New-Item -Path ("$steamroot\" + 'swsdcs.steam') -Value $workshopScript -Force

$steamCMD = Start-Process -FilePath $steamroot\steamcmd.exe -Verb runAs -ArgumentList ("+runscript " +("$steamroot\" + 'swsdcs.steam')) -passthru
Wait-Process $steamCMD.Id
