﻿. C:\arma3\scripts\scriptlibrary.ps1
. C:\arma3\scripts\serverconfig.ps1

Clear-Host
$selection = $false
do{
@"
1. Add/Update a mod
2. Update Arma 3
3. Add new server
4. Edit server configs
5. Start Server
6. Start Headless Client
7. Exit

"@
$Option = Read-Host "Select an option above [1-6] " 
switch ($option){

"1" { #Add/Update a mod
        & "$arma3root\scripts\UpdateArma3Mods.ps1"
    $selection = $false}
"2" { #Update Arma 3
        & "$arma3root\scripts\UpdateArma3.ps1"
    $selection = $false}
"3" { #Install New Server
        $warning = $null
        do{#Verifying before creating new server instance
        Clear-Host;"WARNING!!! It is important to update the config with the new server info before continuing."
        $warning = Read-Host "Are you ready to continue? [Y/N]"
        if(!($warning -eq 'Y') -and !($warning -eq 'N')){clear-host ; "Please type 'Y' for Yes or 'N' for No.";Start-Sleep -Seconds 2}
        
        }while(!($warning -eq 'Y') -and !($warning -eq 'N'))
        if($warning -eq 'Y') {&"$arma3root\scripts\CreateNewArma3Server.ps1";"Completed";Start-Sleep -Seconds 3
            $warning = $null
            do{#Verifying before creating mods on new server instance
            Clear-Host;"WARNING!!! It is important to update the config with the mod info for the new server before continuing."
            $warning = Read-Host "Are you ready to add the mods now? [Y/N]"
            if(!($warning -eq 'Y') -and !($warning -eq 'N')){clear-host ; "Please type 'Y' for Yes or 'N' for No.";Start-Sleep -Seconds 2}     
            }while(!($warning -eq 'Y') -and !($warning -eq 'N'))
            if($warning -eq 'Y') {&"$arma3root\scripts\ServerModSymlink.ps1";"Completed";Start-Sleep -Seconds 3
            }else{"Exiting....";Start-Sleep -Seconds 2}
        }else{"Exiting....";Start-Sleep -Seconds 2}
    $selection = $false}
"4" { #Edit server config
    start "$arma3root\scripts\serverconfig.ps1"
    $selection = $false}
"5" { #Start Server
    & "$arma3root\scripts\StartServer.ps1"
    $selection = $false}
"6" { #Start Headless
    & "$arma3root\scripts\Headless.ps1"
    $selection = $false}
"7" { #Exit Script
    $selection = $true}
default {Clear-Host;"Invalid option, please select an option from the list.";Start-Sleep -Seconds 2;Clear-Host}

}

}while(!$selection)