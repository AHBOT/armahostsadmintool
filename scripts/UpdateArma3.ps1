﻿. C:\arma3\scripts\scriptlibrary.ps1
. C:\arma3\scripts\serverconfig.ps1

$Continue = $true
do{
$workshopScript = @"
$steamCredential
app_update 233780 validate
exit
"@
New-Item -Path ("$steamroot\" + 'swsdcs.steam') -Value $workshopScript -Force

$steamCMD = Start-Process -FilePath $steamroot\steamcmd.exe -Verb runAs -ArgumentList ("+runscript " +("$steamroot" + '\swsdcs.steam')) -passthru
Wait-Process $steamCMD.Id

$result = $null
do{
$result = Read-Host "Did Arma 3 download properly? [Y/N]"
}while(!($result -eq "Y" -or $result -eq "N"))
if($result -eq "Y"){$Continue = $false}else{"Attempting validation and download again"}
}while($continue)


Stop-Process -Name "arma3*"
Start-Sleep -Seconds 3
ForEach ($item in (Get-ChildItem -Path $masterdir -Exclude $filewhitelist)){

Remove-Item -Path $item.fullname -Recurse -Force
}
Move-Item -Path 'C:\arma3\steamcmd\steamapps\common\Arma 3 Server\*' -Destination ($masterdir + '\') -Force
Move-Item -Path "$steamroot\steamapps\common\Arma 3 Server\keys\a3.bikey" -Destination ("$masterdir\keys\a3.bikey") -force