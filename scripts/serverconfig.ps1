﻿#Root of server locations
$arma3root = "C:\arma3"
$steamroot = "$arma3root\steamcmd"
#Location of steamcmd mod downloads
$steamdir = "$steamroot\steamapps\workshop\content\107410"
#Mod Location
$moddir = "$arma3root\mods"
#folders to be linked between master and server instance
$symfolders = @("mpmissions","tadst","addons","argo","curator","dll","dta","expansion","heli","jets","kart","mark","orange","tacops","tank")
#files to be linked between master and server instance
$symfiles = @("tadst.exe","arma3server.exe","arma3server_readme.txt","arma3server_x64.exe","msvcr100.dll","openssl_license.txt","steam.dll","steamclient.dll","steamclient64.dll","steam_api.dll","steam_api64.dll","steam_appid.txt","tier0_s.dll","tier0_s64.dll","vstdlib_s.dll","vstdlib_s64.dll","battleye\beserver_x64.dll","battleye\beserver.dll","keys\a3.bikey")
#Location of the master directory
$masterdir = $arma3root + "\master"
#Folders to be created in each server instancew
$createfolders = @("","battleye","keys")
#Steam Credentials
$steamCredential = 'login smokinawaffle771 Samsungbleed7'
#Files not to be touched when updating Arma 3
$filewhitelist = @("TADST.exe","TADST","mpmissions","keys")
#Download URLS
$steamurl = 'https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip'
$tadsturl = 'https://github.com/jymden/tadst/archive/master.zip'
$dotnetfw = 'https://download.microsoft.com/download/B/A/4/BA4A7E71-2906-4B2D-A0E1-80CF16844F5F/dotNetFx45_Full_setup.exe'
#Config URL
$dotnetbuild = 'C:\Windows\Microsoft.NET\Framework64'
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
 Add-Type -AssemblyName System.web
#Database Info
# Connection Variables 
$user = 'root' 
$pass = 'k9DirOV[X7UL#vCa(hT/W4P{l&gqmk^Q' 
$database = '' #not needed if you declare the database in queries
$MySQLHost = 'localhost' 

# Connect to MySQL Database 
$conn = Connect-MySQL $user $pass $MySQLHost $database
#Servers listed here must be in same order as in $ServerMods
#Remember to add a modlist to $ServerMods if you add a new server
$Servers = @(
"server1"
)

$ServerMods = @(
<# Unused with vanilla server
(#server1 modlist
("ace","463939057"),
("ACECompAFRF","773131200"),
("ACECompUSAF","773125288"),
("ACECompGREF","884966711"),
("ACEAdvMed","1353873848"),
("AdvOrd","1377685826"),
("AdvWeapMount","1378046829"),
("AIAccFix","619834340"),
("BloodLust","667953829"),
("CBA","450814997"),
("ChernarusRedux","1128256978"),
("CUPTerrainsCore","583496184"),
("CUPTerrainsMaps","583544987"),
("dznExtJam","1379304937"),
("EnhancedMovement","333310405"),
("Immerse","825172265"),
("Jbad","520618345"),
("ACELoot2Veh","1439779114"),
("LYTHIUM","909547724"),
("ProjOpfor","735566597"),
("RHSAFRF","843425103"),
("RHSGREF","843593391"),
("RHSSAF","843632231"),
("RHSUSAF","843577117"),
("ShackTacUI","498740884"),
("SMA","699630614"),
("Suppress","825174634"),
("tfar","620019431"),
("Vcom","721359761"),
("ACEX","708250744"),
("AcePT","846847515")
),
<#end server1#>
<#end server3#>
(("","")))#IGNORE LINE







<#
COMPLETE MODLIST FOR REFERENCE

Shortened
("ace","463939057"),
("ACECompAFRF","773131200"),
("ACECompUSAF","773125288"),
("ACECompGREF","884966711"),
("ACEAdvMed","1353873848"),
("AdvOrd","1377685826"),
("AdvWeapMount","1378046829"),
("AIAccFix","619834340"),
("BloodLust","667953829"),
("CBA","450814997"),
("ChernarusRedux","1128256978"),
("CUPTerrainsCore","583496184"),
("CUPTerrainsMaps","583544987"),
("dznExtJam","1379304937"),
("EnhancedMovement","333310405"),
("Immerse","825172265"),
("Jbad","520618345"),
("ACELoot2Veh","1439779114"),
("LYTHIUM","909547724"),
("ProjOpfor","735566597"),
("RHSAFRF","843425103"),
("RHSGREF","843593391"),
("RHSSAF","843632231"),
("RHSUSAF","843577117"),
("ShackTacUI","498740884"),
("SMA","699630614"),
("Suppress","825174634"),
("tfar","620019431"),
("Vcom","721359761"),
("ACEX","708250744"),
("AcePT","846847515")

Untouced
("ace","463939057"),
("ACE Compat - RHS Armed Forces of the Russian Federation","773131200"),
("ACE Compat - RHS United States Armed Forces","773125288"),
("ACE Compat - RHS: GREF","884966711"),
("ADV - ACE Medical","1353873848"),
("Advanced Ordnance","1377685826"),
("Advanced Weapon Mounting","1378046829"),
("Ai accuracy fix","619834340"),
("BloodLust","667953829"),
("CBA_A3","450814997"),
("Chernarus Redux","1128256978"),
("CUP Terrains - Core","583496184"),
("CUP Terrains - Maps","583544987"),
("dzn Extended Jamming","1379304937"),
("Enhanced Movement","333310405"),
("Immerse","825172265"),
("Jbad","520618345"),
("Loot to Vehicle for ACE and Antistasi","1439779114"),
("LYTHIUM","909547724"),
("Project OPFOR","735566597"),
("RHSAFRF","843425103"),
("RHSGREF","843593391"),
("RHSSAF","843632231"),
("RHSUSAF","843577117"),
("ShackTac User Interface","498740884"),
("Specialist Military Arms (SMA) Version 2.7.1","699630614"),
("Suppress","825174634"),
("task_force_radio","620019431")
("Vcom","721359761"),
("ACEX","708250744"),
("AcePT","846847515")
#>