﻿$port = Read-Host "Enter port of main server: [2302]"
$password = Read-Host "Enter the server password: [Blank]"
$servername = Read-Host "Enter the server fodler name: [Server1]"
$HCQty = Read-Host "How many headless clients? [0-3]"

$servers = Get-WmiObject Win32_Process -Filter "name = 'arma3server_x64.exe'" | Select-Object *
$server = ($servers | Where-Object {$_.CommandLine -notlike "*-client*"} | Where-Object {$_.CommandLine -like "*-port=$port*"})
$headless = ($servers | Where-Object {$_.CommandLine -like "*-client*"} | Where-Object {$_.CommandLine -like "*-port=$port*"})
$ServerCheck = ($server | Measure-Object).Count
$HeadlessCheck = ($headless | Measure-Object).Count

if($ServerCheck -eq 0 -and $HeadlessCheck -gt 0){
Stop-Process $headless.ProcessId
"Stopped headless clients without host"
} 
elseif($ServerCheck -eq 0 -and $HeadlessCheck -eq 0){
"No host running on selected port. No headless clients running for selected service."
} 
elseif($ServerCheck -gt 0){
"Server is running, starting headless clients"
    if($HeadlessCheck -gt 0){Stop-Process $headless.ProcessId;"Removed Headless clients"}
    $cmdline = ($server | Where-Object {$_.CommandLine -like "*-port=$port*"}).commandline
    $mods = """" + ($cmdline -split " """ -split """ " | Select-String -Pattern "-mod=") + """"

    for($i=0;$i -lt $HCQty;$i++){
    "Started Headless client $i"
    start-process "C:\arma3\$servername\arma3server_x64.exe" -argumentlist "-client -connect=127.0.0.1 -password=$password -port=$port $mods -profiles=HC" -WorkingDirectory "C:\arma3\$servername"
    Start-Sleep -Seconds 2
    }
}

Start-Sleep -Seconds 2

Clear-Host