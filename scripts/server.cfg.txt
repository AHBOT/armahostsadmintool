//
// server.cfg
//
// comments are written with "//" in front of them.

// GLOBAL SETTINGS

hostname            = "FW.DS";    // The name of the server that will be displayed in the public server list
password            = "";                        // Password to join the server
passwordAdmin       = "R3demption!";                         // Password to become server admin. When you're connected to the server, open the chat and type '#login password'
admins[] = {"76561198008265349"}; 
//reportingIP       = "arma3pc.master.gamespy.com";            // not used anymore in Arma 3
logFile             = "server.log";
verifySignatures    = 0;    // Prevent players with unknown mods from joining the server (best kept at 2 if you want to reduce the number of hackers)
equalModRequired    = 0;    // Prevent players who don't have the exact same mods as the server from joining (best kept at 0)
requiredSecureId    = 2;    // was used to define type of secureID

// WELCOME MESSAGE ("ALL HAIL THE GOAT")
// It can be several lines, separated by comma
// Empty messages "" will not be displayed at all but are only for increasing the interval

motd[] =
{
	"ALL HAIL THE GOAT",
	"TS3 Server:     165.251.164.136:9988",
	"Web:            https://discord.gg/XzzuNd9"
};
motdInterval        = 30;        // Time interval (in seconds) between each message

// JOINING RULES
maxPlayers=50;
kickDuplicate       = 0;         // Each player normally has its own unique ID. If set to 1, players with an ID that is identical to another player will be kicked
//requiredBuild     = 12345;     // Require clients joining to have at least this build version of game, preventing obsolete clients to connect

// VOTING
voteMissionPlayers  = 1;         // Tells the server how many people must connect before displaying the mission selection screen, if you have not already selected a mission in this config
voteThreshold       = 0.33;      // Percentage (0.00 to 1.00) of players needed to vote for something, for example an admin or a new mission, to become effective. Set to 9999 to prevent people from voting random players as admins.

// MISSIONS CYCLE
class Missions
{
	class Mission1
	{
template="Kp_Liberation.Takistan";
		difficulty="Custom";
class Params {};
};
        class Mission2
	{
template="Antistasi.Tanoa";
		difficulty="Custom";
class Params {membership=1;switchComm=0;tkPunish=0;allowPvP=1;pMarkers=1;civTraffic=1;allowFT=0;};
};
        class Mission3
        {
template="A3 - Antistasi Altis.Altis";
		difficulty="Custom";
class Params {membership=1;switchComm=0;tkPunish=0;allowPvP=1;pMarkers=1;civTraffic=1;allowFT=0;};
};
};

missionWhitelist[] = {};

// INGAME SETTINGS
disableVoN = 0;                  // If set to 1, voice chat will be disabled
vonCodecQuality = 10;            // Supports range 1-30; 8kHz is 0-10 (narrowband), 16kHz is 11-20 (wideband), 32kHz is 21-30 (ultrawideband); higher = better sound quality
persistent = 1;                  // If set to 1, missions will continue to run after all players have disconnected
timeStampFormat = "short";       // Set the timestamp format used on each line of the server RPT log file. Possible values are "none" (default), "short", "full".
BattlEye = 1;                    // If set to 0, BattlEye Anti-Cheat will be disabled on the server (not recommended)

// FILE EXTENSIONS
allowedLoadFileExtensions[] =       {"hpp","sqs","sqf","fsm","cpp","paa","txt","xml","inc","ext","sqm","ods","fxy","lip","csv","kb","bik","bikb","html","htm","biedi"}; // only allow files with those extensions to be loaded via loadFile command (since Arma 3 build 1.19.124216) 
allowedPreprocessFileExtensions[] = {"hpp","sqs","sqf","fsm","cpp","paa","txt","xml","inc","ext","sqm","ods","fxy","lip","csv","kb","bik","bikb","html","htm","biedi"}; // only allow files with those extensions to be loaded via preprocessFile/preprocessFileLineNumber commands (since Arma 3 build 1.19.124323)
allowedHTMLLoadExtensions[] =       {"htm","html","xml","txt"}; // only allow files with those extensions to be loaded via HTMLLoad command (since Arma 3 build 1.27.126715)

// SCRIPTING ISSUES
onUserConnected = "";            // command to run when a player connects
onUserDisconnected = "";         // command to run when a player disconnects
doubleIdDetected = "";           // command to run if a player has the same ID as another player in the server

// SIGNATURE VERIFICATION
onUnsignedData = "kick (_this select 0)";    // command to run if a player has unsigned data
onHackedData =  "kick (_this select 0)";     // command to run if a player has data with invalid signatures
onDifferentData = "";                        // command to run if a player has modified data

headlessClients[]={"37.10.125.245","37.10.127.235","37.10.127.207"};
localClient[]={"127.0.0.1","37.10.125.245","37.10.127.235","37.10.127.207"};



serverCommandPassword="Absoluti0n!";