﻿. C:\arma3\scripts\scriptlibrary.ps1
. C:\arma3\scripts\serverconfig.ps1
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$folders = @('keys','master','mods','save','scripts','steamcmd')

foreach($folder in $folders){

New-Item -Path "$arma3root\$folder" -ItemType Directory -Force
}

wget $tadsturl -OutFile "$steamroot\tadst.zip"
wget $steamurl -OutFile "$steamroot\steamcmd.zip"
wget $dotnetfw -OutFile "$steamroot\dotnet45.exe"

Expand-Archive -Path "$steamroot\steamcmd.zip" -DestinationPath "$steamroot" -Force
Expand-Archive -Path "$steamroot\tadst.zip" -DestinationPath "$steamroot" -Force
#INSTALL and MOVE Arma 3 to master folder
$Continue = $true
do{
$workshopScript = @"
$steamCredential
app_update 233780 validate
exit
"@
New-Item -Path ("$steamroot\" + 'swsdcs.steam') -Value $workshopScript -Force

$steamCMD = Start-Process -FilePath "$steamroot\steamcmd.exe" -Verb runAs -ArgumentList ("+runscript " +("$steamroot" + '\swsdcs.steam')) -passthru
Wait-Process $steamCMD.Id

$result = $null
do{
$result = Read-Host "Did Arma 3 download properly? [Y/N]"
}while(!($result -eq "Y" -or $result -eq "N"))
if($result -eq "Y"){$Continue -eq $false}else{"Attempting validation and download again"}
}while($continue)

Move-Item -Path "$steamroot\steamapps\common\Arma 3 Server\*" -Destination ($masterdir + '\') -Force

& $steamroot\dotnet45.exe
start (Get-ChildItem $dotnetbuild -Filter 'msbuild.exe' -Recurse).FullName -ArgumentList "$steamroot\tadst-master\TADST.sln"

Copy-Item -Path $steamroot\tadst-master\TADST\bin\Debug\TADST.exe -Destination $masterdir
Set-Location $masterdir
& $masterdir\tadst.exe
Start-Sleep -Seconds 3
Stop-Process -Name "TADST*"

New-NetFirewallRule -Name Arma3Master -DisplayName "Arma3Master" -Enabled True -Direction Inbound -Protocol ANY -Action Allow -Profile ANY -Program $masterdir\arma3server.exe
New-NetFirewallRule -Name Arma3Masterx64 -DisplayName "Arma3Masterx64" -Enabled True -Direction Inbound -Protocol ANY -Action Allow -Profile ANY -Program $masterdir\arma3server_x64.exe
